using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;

namespace BlazorClient.Pages;

public class Login : PageModel
{
    public async Task OnGet(string redirectUri)
    {
        //If redirect is empty, redirect to dashboard (this avoids an endless loop)
        if (string.IsNullOrEmpty(redirectUri)) redirectUri = "/";

        await HttpContext.ChallengeAsync(OpenIdConnectDefaults.AuthenticationScheme, new AuthenticationProperties { RedirectUri = redirectUri });
    }
}