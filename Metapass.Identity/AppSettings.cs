﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Metapass.Identity
{
    public class AppSettings
    {
        public AppSettings()
        {
            Connector = new Connector();
        }
        public string AppName { get; set; } = "Metapass";
        public string AdministratorUsername { get; set; }
        public string AdministratorPassword { get; set; }
        public AccountRegistry AccountRegistry { get; set; }
        public Event Event { get; set; }
        public OpenIdConnect OpenIdConnect { get; set; }
        public Connector Connector { get; set; }
    }
    public class AccountRegistry
    {
        public string BaseEndpoint { get; set; } = "http://localhost:5001";
        public bool VerifyEmailRegisteredEnabled { get; set; } = false;
        public string VerifyEmailRegisteredEndpoint { get; set; } = "/Registry/Emails";
    }

    public class Event
    {
        public string BaseEndpoint { get; set; } = "http://localhost:5001";
        public bool UserRegisteredEventEnabled { get; set; } = false;
        public string UserRegisteredEventEndpoint { get; set; } = "/api/v1/EventBus/UserRegistered";
    }

    public class OpenIdConnect
    {
        public string WebClientId { get; set; }
        public string WebClientUrl { get; set; }
        public string ManagerClientId { get; set; }
        public string ManagerClientUrl { get; set; }
        public string ApiClientId { get; set; }
        public string ApiClientSecret { get; set; }
        public string PlatformResources { get; set; }
        public string PlatformScopes { get; set; }
        public string IndividualScopes { get; set; }
        public string BusinessScopes { get; set; }
    }
    public class Connector
    {
        public Connector()
        {
            EmailConfig = new EmailConfig();
        }
        public EmailConfig EmailConfig { get; set; }

    }
    public class EmailConfig
    {
        public string Smtp { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}