﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
#nullable disable

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Metapass.Identity.DataModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Metapass.Identity.Areas.Identity.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<User> _signInManager;
        private readonly AppSettings _appSettings;
        
        public RegisterModel(SignInManager<User> signInManager, IOptionsSnapshot<AppSettings> appSettings)
        {
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        [BindProperty]
        public InputModel Input { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public class InputModel
        {
            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Required]
            [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$", ErrorMessage = "Password must be at least 8 characters in length and contain at least 1 lower case letter, 1 upper case letter, 1 numeric character and 1 special character.")]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }


        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                HttpContext.Session.SetString("user.email", Input.Email);
                HttpContext.Session.SetString("user.password", Input.Password);
                HttpContext.Session.SetString("user.security_code", "0");
                HttpContext.Session.SetInt32("user.security_code_resent_counter", 0);
                
                return RedirectToPage("RegisterVerifyEmail");
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        #region Helpers
        
        private async Task<(int, CheckEmailRegisteredResponse)> PlatformCheckEmailRegisteredAsync(string emailAddress)
        {
            int httpStatusCode = 500;
            CheckEmailRegisteredResponse checkEmailRegisteredResponse = new CheckEmailRegisteredResponse();
            
            if (_appSettings.AccountRegistry.VerifyEmailRegisteredEnabled)
            {
                HttpClient httpClient = new HttpClient();
                httpClient.BaseAddress = new Uri(_appSettings.AccountRegistry.BaseEndpoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage httpResponse = new HttpResponseMessage();

                //Set Basic Authorization
                var byteArray = Encoding.UTF8.GetBytes(_appSettings.OpenIdConnect.ApiClientId + ":" + _appSettings.OpenIdConnect.ApiClientSecret);
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                //GET Method  
                httpResponse = await httpClient.GetAsync(_appSettings.AccountRegistry.VerifyEmailRegisteredEndpoint + "/" + emailAddress);

                try
                {
                    //Handle Response
                    var responseString = await httpResponse.Content.ReadAsStringAsync();

                    httpStatusCode = (int)httpResponse.StatusCode;
            
                    if (httpResponse.IsSuccessStatusCode)
                    {
                        checkEmailRegisteredResponse = JsonConvert.DeserializeObject<CheckEmailRegisteredResponse>(responseString);
                    }
                }
                catch (Exception e)
                {
                    checkEmailRegisteredResponse.Success = false;
                    return (500, checkEmailRegisteredResponse);
                }
            }
            return (httpStatusCode, checkEmailRegisteredResponse);
        }

        #endregion Helpers

        #region ApiClassModels

        public class CheckEmailRegisteredResponse
        {
            [Required]
            public bool Success { get; set; }
            [Required]
            public DateTime Timestamp { get; set; } = DateTime.UtcNow;
            public bool? Registered { get; set; }
        }

        #endregion ApiClassModels
    }
}
