﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;

namespace Metapass.Identity.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterThankYouModel : PageModel
    {
        protected readonly AppSettings _appSettings;
        public RegisterThankYouModel(IOptionsSnapshot<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public void OnGet()
        {
            try
            {
                string username = TempData["Username"].ToString();
            }
            catch (Exception e)
            {
                RedirectToPage("Register");
            }

            ViewData["Username"] = TempData["Username"];
            ViewData["Email"] = TempData["Email"];
            ViewData["DashboardEndpoint"] = _appSettings.OpenIdConnect.WebClientUrl;


        }
    }
}
