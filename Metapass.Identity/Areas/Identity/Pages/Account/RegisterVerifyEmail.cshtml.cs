using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Metapass.Identity.Data;
using Metapass.Identity.DataModels;
using Metapass.Identity.Helpers;
using Metapass.Identity.Models;
using Metapass.Identity.Models.Events.Users;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Metapass.Identity.Areas.Identity.Pages.Account
{
    public class RegisterVerifyEmailModel : PageModel
    {
        private readonly ApplicationDbContext _applicationContext;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly AppSettings _appSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<RegisterVerifyEmailModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public RegisterVerifyEmailModel(
            ApplicationDbContext applicationContext,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IOptionsSnapshot<AppSettings> appSettings,
            IHttpContextAccessor httpContextAccessor,
            ILogger<RegisterVerifyEmailModel> logger,
            IEmailSender emailSender,
            IWebHostEnvironment hostingEnvironment)
        {
            _applicationContext = applicationContext;
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            _emailSender = emailSender;
            _hostingEnvironment = hostingEnvironment;
        }


        [TempData] public string StatusMessage { get; set; }

        [BindProperty] public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [StringLength(6, ErrorMessage = "Invalid security code. Please enter a valid security code.",
                MinimumLength = 6)]
            [Display(Name = "Security code")]
            public string SecurityCode { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            string userEmail = HttpContext.Session.GetString("user.email");
            if (userEmail == null)
            {
                return RedirectToPage("./Register");
            }

            //Check user was redirected from register page
            if (HttpContext.Session.GetString("user.security_code") == "0")
            {
                string userSecurityCode = await GenerateSecurityCodeAsync();
                await SendSecurityCode(userEmail, userSecurityCode);
                HttpContext.Session.SetString("user.security_code", userSecurityCode);
            }
            else
            {
                return RedirectToPage("./Register");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            #region Check Session is valid

            string userEmail = HttpContext.Session.GetString("user.email");
            if (string.IsNullOrEmpty(userEmail))
            {
                return RedirectToPage("./Register");
            }

            #endregion Check Session is valid

            //Security Code invalid
            if (Input.SecurityCode != HttpContext.Session.GetString("user.security_code"))
            {
                ModelState.AddModelError("SecurityCode", "Invalid security code. Please enter a valid security code.");
                return Page();
            }

            if (ModelState.IsValid)
            {
                #region Create user

                string newUsername = await GenerateUsernameAsync();
                Guid userId = Guid.NewGuid();
                Guid userAccountApplicationId = Guid.NewGuid();
                var userData = new User
                {
                    Id = userId,
                    UserName = newUsername,
                    Email = HttpContext.Session.GetString("user.email").ToLower(),
                    Name = HttpContext.Session.GetString("user.email").ToLower(),
                    //Account
                    AccountId = userId,
                    AccountNumber = await GenerateMetapassAccountNumberAsync(),
                    AccountStatusCode = "incomplete",
                    ApplicationId = userAccountApplicationId,
                    //ExternalId 
                    Created = DateTime.UtcNow,
                    Updated = DateTime.UtcNow,
                    EmailConfirmed = true,
                    NormalizedEmail = HttpContext.Session.GetString("user.email").ToUpper(),
                    NormalizedUserName = newUsername.ToUpper(),
                };
                var result = await _userManager.CreateAsync(userData, HttpContext.Session.GetString("user.password"));

                if (result.Succeeded)
                {
                    if (_hostingEnvironment.EnvironmentName == "Development")
                    {
                        _logger.LogInformation("User created a new account with password. {Username} | {Email}",
                            userData.UserName, userData.Email);
                    }
                    else
                    {
                        await _emailSender.SendEmailAsync(userData.Email, "Welcome to " + _appSettings.AppName,
                            $"Thank you for registering with " + _appSettings.AppName +
                            ", please take a few moments to complete your online profile.<br /><br />" +
                            "You can use either of these as your login username:<br />" +
                            $"- {userData.UserName}" + "<br />" +
                            $"- {userData.Email}");
                    }

                    await _signInManager.SignInAsync(userData, isPersistent: false);

                    //Set data for display via RegisterThankYou page
                    TempData["Username"] = userData.UserName;
                    TempData["Email"] = userData.Email;

                    //Remove the session keys
                    HttpContext.Session.Remove("user.email");
                    HttpContext.Session.Remove("user.password");
                    HttpContext.Session.Remove("user.security_code");
                    HttpContext.Session.Remove("user.security_code_resent_counter");

                    return RedirectToPage("RegisterThankYou");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }

                #endregion Create user
            }

            return Page();
        }

        //TODO: Implement resend security code
        public async Task<IActionResult> OnPostResendSecurityCodeAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            string userEmail = HttpContext.Session.GetString("user.email");
            if (userEmail == null)
            {
                return RedirectToPage("Register");
            }

            int resentCounter = HttpContext.Session.GetInt32("user.security_code_resent_counter") ?? 0;

            if (resentCounter >= 3)
            {
                StatusMessage = "You have reached the maximum number of resend attempts. Please try again later.";
                return Page();
            }

            string userSecurityCode = await GenerateSecurityCodeAsync();
            await SendSecurityCode(userEmail, userSecurityCode);

            resentCounter++;
            HttpContext.Session.SetInt32("user.security_code_resent_counter", resentCounter);
            HttpContext.Session.SetString("user.security_code", userSecurityCode);

            StatusMessage = "Security code sent. Please check your email.";
            return Page();
        }

        #region Helpers

        private async Task<bool> SendSecurityCode(string emailAddress, string securityCode)
        {
            ViewData["UserSecurityCode"] = securityCode;

            if (_hostingEnvironment.EnvironmentName == "Development")
            {
                _logger.LogInformation("Email sent: Security code {SecurityCode} email address {EmailAddress}",
                    securityCode, emailAddress);
            }
            else
            {
                await _emailSender.SendEmailAsync(emailAddress,
                    "(Action Required) Activate Your " + _appSettings.AppName + " Account",
                    $"Please enter this code on the account setup page to finish creating your " +
                    _appSettings.AppName + " account:<br /><br />" +
                    "<b>" + securityCode + "</b><br /><br />" +
                    "If you did not signup for a Metapass account, please ignore this email.");
            }

            return true;
        }

        private async Task<string> GenerateSecurityCodeAsync()
        {
            const string chars = "0123456789";
            char[] stringChars = new char[6];
            string finalString = null;
            bool valid = false;

            do
            {
                Random random = new Random();

                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }

                finalString = new String(stringChars);

                var user = await _userManager.FindByNameAsync(finalString);

                if (user == null)
                {
                    valid = true;
                }
            } while (valid == false);

            return finalString;
        }

        private async Task<string> GenerateUsernameAsync()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//TODO In the future you may need to increase this
            char[] stringChars = new char[12];
            string finalString = null;
            bool valid = false;

            do
            {
                Random random = new Random();

                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }

                finalString = new String(stringChars);

                var user = await _userManager.FindByNameAsync(finalString);

                if (user == null)
                {
                    valid = true;
                }
            } while (valid == false);

            return finalString;
        }

        #endregion Helpers

        #region AccountHelpers

        /// <summary>
        /// Generate Metapass individual account number
        /// </summary>
        /// <returns></returns>
        private async Task<string> GenerateMetapassAccountNumberAsync()
        {
            int startNumber = 6; //6 is for individual account, 7 is for business account

            string newAccountNumber = null;
            newAccountNumber = GenerateAccountNumber(startNumber.ToString());
            bool duplicateAccountNumberFound = false;

            do
            {
                duplicateAccountNumberFound =
                    await _applicationContext.Users.AnyAsync(q => q.AccountNumber == newAccountNumber);

                newAccountNumber = GenerateAccountNumber(startNumber.ToString());
            } while (duplicateAccountNumberFound);

            return newAccountNumber;
        }

        /// <summary>
        /// Generate 16 digit account number with ending check digit.
        /// </summary>
        /// <param name="accountStartNumber"></param>
        /// <returns></returns>
        private string GenerateAccountNumber(string accountStartNumber = "7")
        {
            string finalResult = null;
            string result = null;

            Random generator = new Random();
            string value1 = generator.Next(0, 9999999).ToString("D7");
            string value2 = generator.Next(0, 99999999).ToString("D8");
            int checkDigit = 0;

            result = accountStartNumber + value1 + value2;

            checkDigit = GenerateChecksumDigit(result);

            finalResult = result + checkDigit.ToString();

            return finalResult;
        }

        /// <summary>
        /// Generate valid numbers by adding a checksum digit using luhn algorithm.
        /// </summary>
        /// <param name="Sequence">Generate a checksum digit from sequence.</param>
        /// <param name="ssCheckDigit">Luhn checksum digit</param>
        private int GenerateChecksumDigit(string Sequence)
        {
            int CheckDigit = 0;

            // convert the sequence to int[]
            char[] charDigits = Sequence.ToCharArray();
            int[] digits = new int[charDigits.Length];

            for (int i = 0; i < charDigits.Length; i++)
            {
                digits[i] = charDigits[i] - '0';
            }

            // our algorithm begins here
            int sum = 0;
            int length = digits.Length;
            for (int i = 0; i < length; i++)
            {
                // get digits in reverse order
                int digit = digits[length - i - 1];

                // multiply every 2nd number with 2
                if (i % 2 == 1)
                {
                    digit *= 2;
                }

                sum += digit > 9 ? digit - 9 : digit;
            }

            CheckDigit = sum % 10;

            return CheckDigit;
        } // GenerateChecksumDigit

        #endregion AccountHelpers

        /// <summary>
        /// Post user registered event to account.api
        /// </summary>
        /// <param name="userRegisteredEvent"></param>
        private async Task PublishUserRegisteredEvent(UserRegisteredEvent userRegisteredEvent)
        {
            #region Publish UserRegisterEvent

            if (_appSettings.Event.UserRegisteredEventEnabled)
            {
                using (var httpClient = new System.Net.Http.HttpClient())
                {
                    httpClient.BaseAddress = new Uri(_appSettings.Event.BaseEndpoint);
                    
                    var byteArray = Encoding.UTF8.GetBytes(_appSettings.OpenIdConnect.ApiClientId + ":" +
                                                           _appSettings.OpenIdConnect.ApiClientSecret);

                    httpClient.DefaultRequestHeaders.Authorization =
                        new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    string json = JsonConvert.SerializeObject(userRegisteredEvent);
                    StringContent data = new StringContent(json, Encoding.UTF8, "application/json");

                    //POST Method
                    var httpResponse = await httpClient.PostAsync(_appSettings.Event.UserRegisteredEventEndpoint, data);

                    if (httpResponse.IsSuccessStatusCode)
                    {
                    }
                }
            }
            
            #endregion Publish UserRegisterEvent
        }
    }
}