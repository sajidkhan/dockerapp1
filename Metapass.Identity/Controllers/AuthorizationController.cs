﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Metapass.Identity.DataModels;
using Metapass.Identity.Helpers;
using Metapass.Identity.Models.Authorization;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using OpenIddict.Abstractions;
using OpenIddict.Core;
using OpenIddict.EntityFrameworkCore.Models;
using OpenIddict.Server.AspNetCore;
using static Metapass.Identity.Data.ApplicationDbContext;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace Metapass.Identity.Controllers
{
    public class AuthorizationController : Controller
    {
        private readonly OpenIddictApplicationManager<ApplicationClient> _applicationManager;
        private readonly OpenIddictAuthorizationManager<ApplicationAuthorization> _authorizationManager;
        private readonly OpenIddictScopeManager<ApplicationScope> _scopeManager;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        protected readonly AppSettings _appSettings;
        
        public AuthorizationController(
            OpenIddictApplicationManager<ApplicationClient> applicationManager,
            OpenIddictAuthorizationManager<ApplicationAuthorization> authorizationManager,
            OpenIddictScopeManager<ApplicationScope> scopeManager,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            IOptionsSnapshot<AppSettings> appSettings)
        {
            _applicationManager = applicationManager;
            _authorizationManager = authorizationManager;
            _scopeManager = scopeManager;
            _signInManager = signInManager;
            _userManager = userManager;
            _appSettings = appSettings.Value;
        }

        [HttpGet("~/connect/authorize")]
        [HttpPost("~/connect/authorize")]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Authorize()
        {
            var request = HttpContext.GetOpenIddictServerRequest() ??
                throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

            // If prompt=login was specified by the client application,
            // immediately return the user agent to the login page.
            if (request.HasPrompt(Prompts.Login))
            {
                // To avoid endless login -> authorization redirects, the prompt=login flag
                // is removed from the authorization request payload before redirecting the user.
                var prompt = string.Join(" ", request.GetPrompts().Remove(Prompts.Login));

                var parameters = Request.HasFormContentType ?
                    Request.Form.Where(parameter => parameter.Key != Parameters.Prompt).ToList() :
                    Request.Query.Where(parameter => parameter.Key != Parameters.Prompt).ToList();

                parameters.Add(KeyValuePair.Create(Parameters.Prompt, new StringValues(prompt)));

                return Challenge(
                    authenticationSchemes: IdentityConstants.ApplicationScheme,
                    properties: new AuthenticationProperties
                    {
                        RedirectUri = Request.PathBase + Request.Path + QueryString.Create(parameters)
                    });
            }
            
        // Retrieve the user principal stored in the authentication cookie.
        // If a max_age parameter was provided, ensure that the cookie is not too old.
        // If the user principal can't be extracted or the cookie is too old, redirect the user to the login page.
        var result = await HttpContext.AuthenticateAsync(IdentityConstants.ApplicationScheme);
        if (result == null || !result.Succeeded || (request.MaxAge != null && result.Properties?.IssuedUtc != null &&
            DateTimeOffset.UtcNow - result.Properties.IssuedUtc > TimeSpan.FromSeconds(request.MaxAge.Value)))
            {
                // If the client application requested promptless authentication,
                // return an error indicating that the user is not logged in.
                if (request.HasPrompt(Prompts.None))
                {
                    return Forbid(
                        authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                        properties: new AuthenticationProperties(new Dictionary<string, string>
                        {
                            [OpenIddictServerAspNetCoreConstants.Properties.Error] = Errors.LoginRequired,
                            [OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription] = "The user is not logged in."
                        }));
                }

                return Challenge(
                    authenticationSchemes: IdentityConstants.ApplicationScheme,
                    properties: new AuthenticationProperties
                    {
                        RedirectUri = Request.PathBase + Request.Path + QueryString.Create(
                            Request.HasFormContentType ? Request.Form.ToList() : Request.Query.ToList())
                    });
            }

            // Retrieve the profile of the logged in user.
            var user = await _userManager.GetUserAsync(result.Principal) ??
                throw new InvalidOperationException("The user details cannot be retrieved.");

            // Retrieve the application details from the database.
            var application = await _applicationManager.FindByClientIdAsync(request.ClientId) ??
                throw new InvalidOperationException("Details concerning the calling client application cannot be found.");

            // Retrieve the permanent authorizations associated with the user and the calling client application.
            var authorizations = await _authorizationManager.FindAsync(
                subject: await _userManager.GetUserIdAsync(user),
                client : await _applicationManager.GetIdAsync(application),
                status : Statuses.Valid,
                type   : AuthorizationTypes.Permanent,
                scopes : request.GetScopes()).ToListAsync();

            switch (await _applicationManager.GetConsentTypeAsync(application))
            {
                // If the consent is external (e.g when authorizations are granted by a sysadmin),
                // immediately return an error if no authorization can be found in the database.
                case ConsentTypes.External when !authorizations.Any():
                    return Forbid(
                        authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                        properties: new AuthenticationProperties(new Dictionary<string, string>
                        {
                            [OpenIddictServerAspNetCoreConstants.Properties.Error] = Errors.ConsentRequired,
                            [OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription] =
                                "The logged in user is not allowed to access this client application."
                        }));

                // If the consent is implicit or if an authorization was found,
                // return an authorization response without displaying the consent form.
                case ConsentTypes.Implicit:
                case ConsentTypes.External when authorizations.Any():
                case ConsentTypes.Explicit when authorizations.Any() && !request.HasPrompt(Prompts.Consent):
                    var principal = await _signInManager.CreateUserPrincipalAsync(user);

                    // Note: in this sample, the granted scopes match the requested scope
                    // but you may want to allow the user to uncheck specific scopes.
                    // For that, simply restrict the list of scopes before calling SetScopes.
                    principal.SetScopes(request.GetScopes());
                    principal.SetResources(await _scopeManager.ListResourcesAsync(principal.GetScopes()).ToListAsync());

                    // Automatically create a permanent authorization to avoid requiring explicit consent
                    // for future authorization or token requests containing the same scopes.
                    var authorization = authorizations.LastOrDefault();
                    if (authorization == null)
                    {
                        authorization = await _authorizationManager.CreateAsync(
                            principal: principal,
                            subject  : await _userManager.GetUserIdAsync(user),
                            client   : await _applicationManager.GetIdAsync(application),
                            type     : AuthorizationTypes.Permanent,
                            scopes   : principal.GetScopes());
                    }

                    principal.SetAuthorizationId(await _authorizationManager.GetIdAsync(authorization));

                    foreach (var claim in principal.Claims)
                    {
                        claim.SetDestinations(GetDestinations(claim, principal));
                    }

                    return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);

                // At this point, no authorization was found in the database and an error must be returned
                // if the client application specified prompt=none in the authorization request.
                case ConsentTypes.Explicit   when request.HasPrompt(Prompts.None):
                case ConsentTypes.Systematic when request.HasPrompt(Prompts.None):
                    return Forbid(
                        authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                        properties: new AuthenticationProperties(new Dictionary<string, string>
                        {
                            [OpenIddictServerAspNetCoreConstants.Properties.Error] = Errors.ConsentRequired,
                            [OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription] =
                                "Interactive user consent is required."
                        }));

                // In every other case, render the consent form.
                default:
                    return View(new AuthorizeViewModel
                    {
                        ApplicationName = await _applicationManager.GetDisplayNameAsync(application),
                        Scope = request.Scope
                    });
            }
        }

        [Authorize, FormValueRequired("submit.Accept")]
        [HttpPost("~/connect/authorize"), ValidateAntiForgeryToken]
        public async Task<IActionResult> Accept()
        {
            var request = HttpContext.GetOpenIddictServerRequest() ??
                throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

            // Retrieve the profile of the logged in user.
            var user = await _userManager.GetUserAsync(User) ??
                throw new InvalidOperationException("The user details cannot be retrieved.");

            // Retrieve the application details from the database.
            var application = await _applicationManager.FindByClientIdAsync(request.ClientId) ??
                throw new InvalidOperationException("Details concerning the calling client application cannot be found.");

            // Retrieve the permanent authorizations associated with the user and the calling client application.
            var authorizations = await _authorizationManager.FindAsync(
                subject: await _userManager.GetUserIdAsync(user),
                client : await _applicationManager.GetIdAsync(application),
                status : Statuses.Valid,
                type   : AuthorizationTypes.Permanent,
                scopes : request.GetScopes()).ToListAsync();

            // Note: the same check is already made in the other action but is repeated
            // here to ensure a malicious user can't abuse this POST-only endpoint and
            // force it to return a valid response without the external authorization.
            if (!authorizations.Any() && await _applicationManager.HasConsentTypeAsync(application, ConsentTypes.External))
            {
                return Forbid(
                    authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                    properties: new AuthenticationProperties(new Dictionary<string, string>
                    {
                        [OpenIddictServerAspNetCoreConstants.Properties.Error] = Errors.ConsentRequired,
                        [OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription] =
                            "The logged in user is not allowed to access this client application."
                    }));
            }

            var principal = await _signInManager.CreateUserPrincipalAsync(user);

            // Note: in this sample, the granted scopes match the requested scope
            // but you may want to allow the user to uncheck specific scopes.
            // For that, simply restrict the list of scopes before calling SetScopes.
            principal.SetScopes(request.GetScopes());
            principal.SetResources(await _scopeManager.ListResourcesAsync(principal.GetScopes()).ToListAsync());

            // Automatically create a permanent authorization to avoid requiring explicit consent
            // for future authorization or token requests containing the same scopes.
            var authorization = authorizations.LastOrDefault();
            if (authorization == null)
            {
                authorization = await _authorizationManager.CreateAsync(
                    principal: principal,
                    subject  : await _userManager.GetUserIdAsync(user),
                    client   : await _applicationManager.GetIdAsync(application),
                    type     : AuthorizationTypes.Permanent,
                    scopes   : principal.GetScopes());
            }

            principal.SetAuthorizationId(await _authorizationManager.GetIdAsync(authorization));

            foreach (var claim in principal.Claims)
            {
                claim.SetDestinations(GetDestinations(claim, principal));
            }

            // Returning a SignInResult will ask OpenIddict to issue the appropriate access/identity tokens.
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
        }

        [Authorize, FormValueRequired("submit.Deny")]
        [HttpPost("~/connect/authorize"), ValidateAntiForgeryToken]
        // Notify OpenIddict that the authorization grant has been denied by the resource owner
        // to redirect the user agent to the client application using the appropriate response_mode.
        public IActionResult Deny() => Forbid(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);

        [HttpGet("~/connect/logout")]
        public IActionResult Logout() => View();

        [ActionName(nameof(Logout)), HttpPost("~/connect/logout"), ValidateAntiForgeryToken]
        public async Task<IActionResult> LogoutPost()
        {
            // Ask ASP.NET Core Identity to delete the local and external cookies created
            // when the user agent is redirected from the external identity provider
            // after a successful authentication flow (e.g Google or Facebook).
            await _signInManager.SignOutAsync();

            // Returning a SignOutResult will ask OpenIddict to redirect the user agent
            // to the post_logout_redirect_uri specified by the client application or to
            // the RedirectUri specified in the authentication properties if none was set.
            return SignOut(
                authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                properties: new AuthenticationProperties
                {
                    RedirectUri = "/"
                });
        }

        [HttpPost("~/connect/token"), Produces("application/json")]
        public async Task<IActionResult> Exchange()
        {
            var request = HttpContext.GetOpenIddictServerRequest() ??
                throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

            #region Authorization Code and RefreshToken
            if (request.IsAuthorizationCodeGrantType() || request.IsRefreshTokenGrantType())
            {
                // Retrieve the claims principal stored in the authorization code/device code/refresh token.
                var principal = (await HttpContext.AuthenticateAsync(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme)).Principal;

                // Retrieve the user profile corresponding to the authorization code/refresh token.
                // Note: if you want to automatically invalidate the authorization code/refresh token
                // when the user password/roles change, use the following line instead:
                // var user = _signInManager.ValidateSecurityStampAsync(info.Principal);
                var user = await _userManager.GetUserAsync(principal);
                if (user == null)
                {
                    return Forbid(
                        authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                        properties: new AuthenticationProperties(new Dictionary<string, string>
                        {
                            [OpenIddictServerAspNetCoreConstants.Properties.Error] = Errors.InvalidGrant,
                            [OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription] = "The token is no longer valid."
                        }));
                }

                // Ensure the user is still allowed to sign in.
                if (!await _signInManager.CanSignInAsync(user))
                {
                    return Forbid(
                        authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                        properties: new AuthenticationProperties(new Dictionary<string, string>
                        {
                            [OpenIddictServerAspNetCoreConstants.Properties.Error] = Errors.InvalidGrant,
                            [OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription] = "The user is no longer allowed to sign in."
                        }));
                }

                #region Add Custom Claims (IdentityToken)
                principal.SetClaim("user_id", user.Id.ToString());
                if(!string.IsNullOrEmpty(user.FirstName)) principal.SetClaim("first_name", user.FirstName);
                if (!string.IsNullOrEmpty(user.LastName)) principal.SetClaim("last_name", user.LastName);
                principal.SetClaim("name", user.Name);
                principal.SetClaim("username", user.UserName);
                principal.SetClaim("email", user.Email);

                //Account Information (account is only available after it has been created via account microservice service)
                if (user.Id != Guid.Empty) principal.SetClaim("account_id", user.Id.ToString());
                if (!string.IsNullOrEmpty(user.Name)) principal.SetClaim("account_name", user.Name);
                if (!string.IsNullOrEmpty(user.AccountNumber)) principal.SetClaim("account_number", user.AccountNumber);
                principal.SetClaim("account_profile_type", "individual");
                if (!string.IsNullOrEmpty(user.AccountStatusCode)) principal.SetClaim("account_status", user.AccountStatusCode);
                if (user.Id != Guid.Empty) principal.SetClaim("account_owner_account_id", user.Id.ToString());
                if (user.Id != Guid.Empty) principal.SetClaim("account_owner_user_id", user.Id.ToString());
                if (user.ApplicationId != Guid.Empty) principal.SetClaim("application_id", user.ApplicationId.ToString());

                foreach (var claim in principal.Claims)
                {
                    claim.SetDestinations(GetDestinations(claim, principal));

                    //Set destination for custom claims
                    if (claim.Type == "user_id" || claim.Type.Contains("account_")) claim.SetDestinations(Destinations.IdentityToken);
                }
                #endregion Add Custom Claims (IdentityToken)

                // Returning a SignInResult will ask OpenIddict to issue the appropriate access/identity tokens.
                return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            }
            #endregion Authorization Code and RefreshToken

            #region Client Credentials
            if (request.IsClientCredentialsGrantType())
            {
                // Note: the client credentials are automatically validated by OpenIddict:
                // if client_id or client_secret are invalid, this action won't be invoked.

                var application = await _applicationManager.FindByClientIdAsync(request.ClientId);
                if (application == null)
                {
                    throw new InvalidOperationException("The application details cannot be found in the database.");
                }

                // Create a new ClaimsIdentity containing the claims that
                // will be used to create an id_token, a token or a code.
                var identity = new ClaimsIdentity(
                    TokenValidationParameters.DefaultAuthenticationType,
                    Claims.Name, Claims.Role);

                // Use the client_id as the subject identifier.
                string clientId = await _applicationManager.GetClientIdAsync(application);
                identity.AddClaim(Claims.Subject, clientId,
                    Destinations.AccessToken, Destinations.IdentityToken);

                identity.AddClaim("client_id", clientId,
                    Destinations.AccessToken, Destinations.IdentityToken);

                //identity.AddClaim(Claims.Name, await _applicationManager.GetDisplayNameAsync(application),
                    //Destinations.AccessToken, Destinations.IdentityToken);

                #region Add Custom Claims to ClientCredentials
                bool addCustomClaims = false;
                var keys = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                foreach (var item in keys)
                {
                    if (item.Key == "set_custom_claims")
                    {
                        addCustomClaims = item.Value == "true" ? true : false;
                    }
                }

                // Add claims sent via form parameters
                // account_id*
                // account_name
                // account_number
                // account_profile_type
                // account_status
                // account_owner_account_id
                // account_owner_user_id*
                // user_id*
                // application_id
                //
                // *madatory input parameters, if missing api will not work
                if (addCustomClaims)
                {
                    foreach (var item in keys)
                    {
                        if (item.Key.ToLower() == "account_id" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("account_id", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "account_name" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("account_name", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "account_number" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("account_number", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "account_profile_type" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("account_profile_type", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "account_status" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("account_status", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "account_owner_account_id" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("account_owner_account_id", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "account_owner_user_id" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("account_owner_user_id", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "user_id" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("user_id", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                        if (item.Key.ToLower() == "application_id" && !string.IsNullOrEmpty(item.Value)) identity.AddClaim("application_id", item.Value, Destinations.AccessToken, Destinations.IdentityToken);
                    }
                }
                #endregion Add Custom Claims to ClientCredentials

                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                List<string> resourceList = new List<string>();
                List<string> audienceList = new List<string>();
                resourceList = _appSettings.OpenIdConnect.PlatformResources.ToLower().Split("|").ToList();
                audienceList = resourceList;

                string[] audiences = audienceList.ToArray();
                resourceList.Add(clientId);
                string[] resources = resourceList.ToArray();

                principal.SetAudiences(audiences);
                principal.SetResources(resources);

                principal.SetScopes(request.GetScopes());

                return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            }
            #endregion Client Credentials

            throw new InvalidOperationException("The specified grant type is not supported.");
        }

        #region Helpers
        private IEnumerable<string> GetDestinations(Claim claim, ClaimsPrincipal principal)
        {
            // Note: by default, claims are NOT automatically included in the access and identity tokens.
            // To allow OpenIddict to serialize them, you must attach them a destination, that specifies
            // whether they should be included in access tokens, in identity tokens or in both.

            switch (claim.Type)
            {
                case Claims.Name:
                    yield return Destinations.AccessToken;

                    if (principal.HasScope(Scopes.Profile))
                        yield return Destinations.IdentityToken;

                    yield break;

                case Claims.Email:
                    yield return Destinations.AccessToken;

                    if (principal.HasScope(Scopes.Email))
                        yield return Destinations.IdentityToken;

                    yield break;

                case Claims.Role:
                    yield return Destinations.AccessToken;

                    if (principal.HasScope(Scopes.Roles))
                        yield return Destinations.IdentityToken;

                    yield break;

                // Never include the security stamp in the access and identity tokens, as it's a secret value.
                case "AspNet.Identity.SecurityStamp": yield break;

                default:
                    yield return Destinations.AccessToken;

                    yield break;
            }
        }
        #endregion Helpers
    }
}