﻿using System.Text;
using Metapass.Identity.Models.Api;
using Metapass.Identity.Models.Api.Users;
using Metapass.Identity.Models.Events.Users;
using Metapass.Identity.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Metapass.Identity.Controllers;

[Consumes("application/json")]
[Produces("application/json")]
[Route("api/v1/[controller]")]
[ApiController]
public class EventListenersController : Controller
{
    private readonly AppSettings _appSettings;
    private readonly IEventListenerService _eventListenerService;

    public EventListenersController(IOptionsSnapshot<AppSettings> appSettings, IEventListenerService eventListenerService)
    {
        _appSettings = appSettings.Value;
        _eventListenerService = eventListenerService;
    }
    
    /// <summary>
    /// Update user's account status via Account.API
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut("UserAccountCreatedEvent")]
    public async Task<IActionResult> UserAccountCreatedEvent([FromBody] UserAccountCreatedEvent request)
    {
        //Check user is authorized
        if (!IsAuthorized())
        {
            return StatusCode(Unauthorized().StatusCode);
        }
        
        var returnedResult = await _eventListenerService.UserAccountCreatedEventAsync(request);

        return StatusCode(returnedResult);
    }
    
    #region Helpers

    private bool IsAuthorized()
    {
        string authHeader = Request.Headers["Authorization"];
        string basicAuthUsername = null;
        string basicAuthPassword = null;
        if (authHeader != null && authHeader.StartsWith("Basic"))
        {
            //Extract credentials
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim(); //Remove Basic
            //Decoding Base64
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            basicAuthUsername = usernamePassword.Substring(0, seperatorIndex); //username
            basicAuthPassword = usernamePassword.Substring(seperatorIndex + 1); //password

            if (basicAuthUsername == _appSettings.OpenIdConnect.ApiClientId &&
                basicAuthPassword == _appSettings.OpenIdConnect.ApiClientSecret)
            {
                return true;
            }
        }

        return false;
    }
    
    #endregion Helpers
}