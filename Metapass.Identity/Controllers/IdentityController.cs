﻿using System.Text;
using Metapass.Identity.Models.Api;
using Metapass.Identity.Models.Api.Users;
using Metapass.Identity.Models.Events.Users;
using Metapass.Identity.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Metapass.Identity.Controllers;

[Consumes("application/json")]
[Produces("application/json")]
[Route("api/v1/Identity")]
[ApiController]
public class IdentityController : Controller
{
    private readonly AppSettings _appSettings;
    private readonly IIdentityRepository _identityRepository;

    public IdentityController(IOptionsSnapshot<AppSettings> appSettings, IIdentityRepository identityRepository)
    {
        _appSettings = appSettings.Value;
        _identityRepository = identityRepository;
    }
    
    /// <summary>
    /// Create user
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost("Users")]
    public async Task<IActionResult> CreateUser([FromBody] CreateUserRequest request)
    {
        //Check user is authorized
        if (!IsAuthorized())
        {
            return StatusCode(Unauthorized().StatusCode);
        }
        
        var returnedResult = await _identityRepository.CreateUserAsync(request);

        return StatusCode(returnedResult.Item1, returnedResult.Item2);
    }

    /// <summary>
    /// Update user's account: name, email
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut("Users/{userId}")]
    public async Task<IActionResult> UpdateUser([FromRoute] Guid userId, [FromBody] UpdateUserRequest request)
    {
        //Check user is authorized
        if (!IsAuthorized())
        {
            return StatusCode(Unauthorized().StatusCode);
        }
        
        var returnedResult = await _identityRepository.UpdateUserAsync(userId, request);

        return StatusCode(returnedResult.Item1, returnedResult.Item2);
    }

    #region Helpers

    private bool IsAuthorized()
    {
        string authHeader = Request.Headers["Authorization"];
        string basicAuthUsername = null;
        string basicAuthPassword = null;
        if (authHeader != null && authHeader.StartsWith("Basic"))
        {
            //Extract credentials
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim(); //Remove Basic
            //Decoding Base64
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            basicAuthUsername = usernamePassword.Substring(0, seperatorIndex); //username
            basicAuthPassword = usernamePassword.Substring(seperatorIndex + 1); //password

            if (basicAuthUsername == _appSettings.OpenIdConnect.ApiClientId &&
                basicAuthPassword == _appSettings.OpenIdConnect.ApiClientSecret)
            {
                return true;
            }
        }

        return false;
    }
    
    #endregion Helpers
}