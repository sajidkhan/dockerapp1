﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Metapass.Identity.DataModels;
using Microsoft.AspNetCore.Identity;
using OpenIddict.EntityFrameworkCore.Models;

namespace Metapass.Identity.Data
{
    public partial class ApplicationDbContext : IdentityDbContext<User, Role, Guid, IdentityUserClaim<Guid>, IdentityUserRole<Guid>, IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.UseOpenIddict<ApplicationClient, ApplicationAuthorization, ApplicationScope, ApplicationToken, Guid>();

            builder.Entity<User>()
            .HasMany(e => e.Claims)
            .WithOne()
            .HasForeignKey(e => e.UserId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<User>()
                .HasMany(e => e.Logins)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<User>()
                .HasMany(e => e.Roles)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<IdentityUserRole<Guid>>().HasKey(p => new { p.UserId, p.RoleId });

            ////Postgresql, we convert Pascal casing to Snake casing
            _FixTableNames(builder);
        }

        public class ApplicationClient : OpenIddictEntityFrameworkCoreApplication<Guid, ApplicationAuthorization, ApplicationToken>
        {

            public bool? IsActive { get; set; }
            public string? Remarks { get; set; }
        }

        public class ApplicationAuthorization : OpenIddictEntityFrameworkCoreAuthorization<Guid, ApplicationClient, ApplicationToken> { }
        public class ApplicationScope : OpenIddictEntityFrameworkCoreScope<Guid> { }
        public class ApplicationToken : OpenIddictEntityFrameworkCoreToken<Guid, ApplicationClient, ApplicationAuthorization> { }

        #region FixTableNames
        private void _FixTableNames(ModelBuilder builder)
        {
            var mapper = new Npgsql.NameTranslation.NpgsqlNullNameTranslator();

            foreach (var entity in builder.Model.GetEntityTypes())
            {
                // modify column names
                //foreach (var property in entity.GetProperties())
                //{
                //    property.SetColumnName(mapper.TranslateMemberName(property.GetColumnName()));
                //}

                // modify table name
                entity.SetTableName(mapper.TranslateMemberName(entity.GetTableName()));

                // move asp_net tables into schema 'identity'
                if (entity.GetTableName().StartsWith("AspNet"))
                {
                    entity.SetTableName(entity.GetTableName().Replace("AspNet", string.Empty));
                    //entity.Relational().Schema = "identity";
                }
            }
        }
        #endregion
    }
}
