﻿using Microsoft.Extensions.Configuration;
using Metapass.Identity.DataModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Metapass.Identity.Data
{
    public class ApplicationDbContextSeed
    {
        private readonly IPasswordHasher<User> _passwordHasher = new PasswordHasher<User>();

        public async Task SeedAsync(ApplicationDbContext context, IConfiguration configuration, IWebHostEnvironment env,
            ILogger<ApplicationDbContextSeed> logger, int? retry = 5)
        {
            int retryForAvaiability = retry.Value;
            string basePath = AppDomain.CurrentDomain.BaseDirectory;

            try
            {
                string administratorUsername = configuration.GetValue<string>("AdministratorUsername");
                string administratorPassword = configuration.GetValue<string>("AdministratorPassword");
                
                if (!context.Users.Any())
                {
                    
                    User user = GetAdminUser(administratorUsername, administratorPassword);
                    context.Users.Add(user);

                    Console.WriteLine(
                        "**********************************************************************" + Environment.NewLine + 
                        Environment.NewLine +
                        "  Admin username: " + administratorUsername + Environment.NewLine +
                        "  Admin password: " + administratorPassword + Environment.NewLine + 
                        Environment.NewLine +
                        "**********************************************************************"
                        );

                    await context.SaveChangesAsync();

                }

                if (!context.Roles.Any())
                {
                    context.Roles.AddRange(GetDefaultRoles());

                    await context.SaveChangesAsync();
                }

                if (!context.UserRoles.Any())
                {
                    //Assign Administrator role to Administrator
                    var userIdData = context.Users.Where(q => q.UserName == administratorUsername).Single().Id;
                    var roleIdData = context.Roles.Where(q => q.Name == "Administrator").Single().Id;

                    UserRole userRoleData = new UserRole()
                    {
                        UserId = userIdData,
                        RoleId = roleIdData
                    };

                    context.UserRoles.Add(userRoleData);

                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                if(retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, configuration, env, logger, retryForAvaiability);
                }
            }
        }

        private User GetAdminUser(string username, string password)
        {
            var userData =
            new User()
            {
                Id = Guid.NewGuid(),
                Name = username,
                PhoneNumber = "",
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
                UserName = username,
                Email = username,
                EmailConfirmed = true,
                NormalizedEmail = username.ToUpper(),
                NormalizedUserName = username.ToUpper(),
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };

            userData.PasswordHash = _passwordHasher.HashPassword(userData, password);

            return userData;
        }
        private IEnumerable<Role> GetDefaultRoles()
        {
            string AdministratorRoleName = "Administrator";
            
            var roleData =
                new Role()
                {
                    Id = Guid.NewGuid(),
                    Name = AdministratorRoleName,
                    Description = AdministratorRoleName + " - Full Access",
                    NormalizedName = AdministratorRoleName.ToUpper(),
                    Reserved = true,
                    ConcurrencyStamp = Guid.NewGuid().ToString("D"),
                };

            return new List<Role>()
            {
                roleData
            };

        }
    }
}
