﻿using Microsoft.Extensions.Configuration;
using Metapass.Identity.DataModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Metapass.Identity.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OpenIddict.Abstractions;
using OpenIddict.Core;
using OpenIddict.EntityFrameworkCore.Models;
using static OpenIddict.Abstractions.OpenIddictConstants;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using static Metapass.Identity.Data.ApplicationDbContext;

namespace Metapass.Identity.Data
{
    public class ConfigurationDbContextSeed
    {
        public async Task IntialiseAsync(IApplicationBuilder app, IConfiguration configuration)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                await context.Database.EnsureCreatedAsync();

                await CreateClientApplicationsAsync();
                await CreateScopesAsync();

                async Task CreateClientApplicationsAsync()
                {
                    //callbacks urls from config:
                    Dictionary<string, string> clientUrls = new Dictionary<string, string>();
                    clientUrls.Add("Manager", configuration.GetValue<string>("OpenIdConnect:ManagerClientUrl"));
                    clientUrls.Add("Web", configuration.GetValue<string>("OpenIdConnect:WebClientUrl"));

                    var openIddictApplicationManager = serviceScope.ServiceProvider.GetRequiredService<OpenIddictApplicationManager<ApplicationClient>>();

                    try
                    {
                        #region Clients
                        //Metapass Platform (Client Credentials): api
                        string apiClientId = configuration.GetValue<string>("OpenIdConnect:ApiClientId");
                        string apiClientSecret = configuration.GetValue<string>("OpenIdConnect:ApiClientSecret");
                        if (await openIddictApplicationManager.FindByClientIdAsync(apiClientId) is null)
                        {
                            var publicClientCredentialOpenIddictApplicationDescriptor = new OpenIddictApplicationDescriptor
                            {
                                ClientId = apiClientId,
                                ClientSecret = apiClientSecret,
                                Permissions =
                                {
                                    Permissions.Endpoints.Token,
                                    Permissions.GrantTypes.ClientCredentials,
                                    //Token Introspection, used by resource servers to obtain information about access tokens. 
                                    //resource servers can find out which user and scopes are associated with the token.
                                    Permissions.Endpoints.Introspection
                                }
                            };

                            //Add Platform Scopes
                            string platformScopes = configuration.GetValue<string>("OpenIdConnect:PlatformScopes");
                            foreach (var scopeItem in GetPlatformApiScopes(platformScopes))
                            {
                                publicClientCredentialOpenIddictApplicationDescriptor.Permissions.Add(scopeItem);
                            }

                            await openIddictApplicationManager.CreateAsync(publicClientCredentialOpenIddictApplicationDescriptor);
                        }

                        //Metapass Web (Implicit): Account dashboard manager
                        string managerClientId = configuration.GetValue<string>("OpenIdConnect:ManagerClientId");
                        if (await openIddictApplicationManager.FindByClientIdAsync(managerClientId) is null)
                        {
                            var managerOpenIddictApplicationDescriptor = new OpenIddictApplicationDescriptor
                            {
                                ClientId = managerClientId,
                                ConsentType = ConsentTypes.Implicit,
                                DisplayName = "Manager",
                                Type = ClientTypes.Public,
                                PostLogoutRedirectUris =
                                {
                                    new Uri($"{clientUrls["Manager"]}/signout-callback-oidc")
                                },
                                RedirectUris =
                                {
                                    new Uri($"{clientUrls["Manager"]}/signin-oidc")
                                },
                                Permissions =
                                {
                                    Permissions.Endpoints.Authorization,
                                    Permissions.Endpoints.Logout,
                                    Permissions.Endpoints.Token,
                                    Permissions.GrantTypes.AuthorizationCode,
                                    Permissions.GrantTypes.RefreshToken,
                                    Permissions.ResponseTypes.Code,
                                    Permissions.Scopes.Email,
                                    Permissions.Scopes.Profile,
                                    Permissions.Scopes.Roles
                                },
                                Requirements =
                                {
                                    Requirements.Features.ProofKeyForCodeExchange
                                }
                            };

                            await openIddictApplicationManager.CreateAsync(managerOpenIddictApplicationDescriptor);
                        }

                        //Metapass Web (Implicit): Account dashboard
                        string webClientId = configuration.GetValue<string>("OpenIdConnect:WebClientId");
                        if (await openIddictApplicationManager.FindByClientIdAsync(webClientId) is null)
                        {
                            var webOpenIddictApplicationDescriptor = new OpenIddictApplicationDescriptor
                            {
                                ClientId = webClientId,
                                ConsentType = ConsentTypes.Implicit,
                                DisplayName = "Web",
                                Type = ClientTypes.Public,
                                PostLogoutRedirectUris =
                                {
                                        new Uri($"{clientUrls["Web"]}/signout-callback-oidc")
                                },
                                RedirectUris =
                                {
                                        new Uri($"{clientUrls["Web"]}/signin-oidc")
                                },
                                Permissions =
                                {
                                    Permissions.Endpoints.Authorization,
                                    Permissions.Endpoints.Logout,
                                    Permissions.Endpoints.Token,
                                    Permissions.GrantTypes.AuthorizationCode,
                                    Permissions.GrantTypes.RefreshToken,
                                    Permissions.ResponseTypes.Code,
                                    Permissions.Scopes.Email,
                                    Permissions.Scopes.Profile,
                                    Permissions.Scopes.Roles
                                },
                                Requirements =
                                {
                                    Requirements.Features.ProofKeyForCodeExchange
                                }
                            };

                            await openIddictApplicationManager.CreateAsync(webOpenIddictApplicationDescriptor);
                        }
                        #endregion Clients
                    }
                    catch (Exception ex)
                    {
                    }
                }

                async Task CreateScopesAsync()
                {
                    try
                    {
                        var openIddictScopeManager = serviceScope.ServiceProvider.GetRequiredService<OpenIddictScopeManager<ApplicationScope>>();

                        string PlatformResourcesString = configuration.GetValue<string>("OpenIdConnect:PlatformResources");
                        string[] platformApiResourceList = PlatformResourcesString.Split('|');

                        foreach (var platformApiResourceItem in platformApiResourceList)
                        {
                            if (await openIddictScopeManager.FindByNameAsync(platformApiResourceItem.ToLower()) == null)
                            {
                                //Scope: Create, modify and delete
                                OpenIddictScopeDescriptor scopeDescriptor = new OpenIddictScopeDescriptor
                                {
                                    Name = platformApiResourceItem.ToLower(),
                                    DisplayName = platformApiResourceItem.ToLower(),
                                    Description = "Create, delete or modify access to '" + platformApiResourceItem.ToLower() + "' API",
                                    Resources =
                                    {
                                        platformApiResourceItem.ToLower()
                                    }
                                };
                                await openIddictScopeManager.CreateAsync(scopeDescriptor);

                                //Scope: Create or modify
                                scopeDescriptor = new OpenIddictScopeDescriptor
                                {
                                    Name = platformApiResourceItem.ToLower() + ".write",
                                    DisplayName = platformApiResourceItem.ToLower() + ".write",
                                    Description = "Create or modify access to '" + platformApiResourceItem.ToLower() + "' API",
                                    Resources =
                                    {
                                        platformApiResourceItem.ToLower()
                                    }
                                };
                                await openIddictScopeManager.CreateAsync(scopeDescriptor);

                                //Scope: Read only
                                scopeDescriptor = new OpenIddictScopeDescriptor
                                {
                                    Name = platformApiResourceItem.ToLower() + ".read",
                                    DisplayName = platformApiResourceItem.ToLower() + ".read",
                                    Description = "Read only access to '" + platformApiResourceItem.ToLower() + "' API",
                                    Resources =
                                    {
                                        platformApiResourceItem.ToLower()
                                    }
                                };
                                await openIddictScopeManager.CreateAsync(scopeDescriptor);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

        }

        #region Helpers
        private List<string> GetPlatformApiScopes(string platformScopesString)
        {
            List<string> returnedPermissions = new List<string>();

            string[] platformScopes = platformScopesString.ToLower().Split('|');

            foreach (var scopeItem in platformScopes)
            {
                returnedPermissions.Add(OpenIddictConstants.Permissions.Prefixes.Scope + scopeItem);
            }

            return returnedPermissions;
        }
        #endregion Helpers
    }
}
