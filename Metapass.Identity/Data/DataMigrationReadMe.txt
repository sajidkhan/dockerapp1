﻿Add Entity Framework Migrations
===============================
Package Manager Console
-----------------------
Add-Migration InitialDbMigration -context "ApplicationDbContext" -OutputDir "Data\Migrations"

CommandLine
-----------
dotnet tool install --global dotnet-ef

dotnet ef migrations add InitialDbMigration --context "ApplicationDbContext" -o "Data\Migrations"