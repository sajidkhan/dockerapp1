﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Metapass.Identity.DataModels
{
    public partial class UserLogin : IdentityUserLogin<Guid>
    {
    }
    public partial class UserRole : IdentityUserRole<Guid>
    {
    }
    public partial class UserClaim : IdentityUserClaim<Guid>
    {
    }
    public partial class Role : IdentityRole<Guid>
    {
        public string? Description { get; set; }

        public bool? Reserved { get; set; }

        public Role() : base()
        { 
        }

        public Role(string roleName)
        {
            Name = roleName;
        }
    }
    public partial class RoleClaim : IdentityRoleClaim<Guid>
    {
    }
    public partial class UserToken : IdentityUserToken<Guid>
    {
    }
}
