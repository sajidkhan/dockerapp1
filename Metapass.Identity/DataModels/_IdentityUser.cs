﻿using System;
using System.Data;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Metapass.Identity.DataModels
{
    public partial class User : IdentityUser<Guid>
    {
        public string? Name { get; set; }  //Can update
        public string? FirstName { get; set; }  //Can update
        public string? LastName { get; set; }  //Can update

        #region Individual Account
        public Guid? AccountId { get; set; }
        public string? AccountNumber { get; set; }
        public string? AccountStatusCode { get; set; } //Can update
        public Guid? ApplicationId { get; set; }
        #endregion Individual Account

        public string? ExternalId { get; set; }
        public string? MetaData { get; set; }
        public DateTime? Synced { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }


        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<IdentityUserRole<Guid>> Roles { get; } = new List<IdentityUserRole<Guid>>();

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<IdentityUserClaim<Guid>> Claims { get; } = new List<IdentityUserClaim<Guid>>();

        /// <summary>
        /// Navigation property for this users login accounts.
        /// </summary>
        public virtual ICollection<IdentityUserLogin<Guid>> Logins { get; } = new List<IdentityUserLogin<Guid>>();
    }
}
