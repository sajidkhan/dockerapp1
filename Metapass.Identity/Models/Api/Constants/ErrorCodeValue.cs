﻿namespace Metapass.Identity.Models.Api.Constants;

/// <summary>
/// The HTTP status error code returned in Response.Errors.Error.Code
/// </summary>
public static class ErrorCodeValue
{
    public const string BadRequest = "400";       
    public const string Unauthorized = "401";
    public const string RequestFailed = "402";
    public const string Forbidden = "403";
    public const string NotFound = "404"; 
    public const string Conflict = "409";
    public const string TooManyRequests = "429";
    public const string ServerError = "500";
    public const string NotImplemented = "501";
    public const string BadGateway = "502";
    public const string ServiceUnavailable = "503";
}