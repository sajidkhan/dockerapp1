﻿namespace Metapass.Identity.Models.Api.Constants;

/// <summary>
/// 400 bad request error types.
/// </summary>
public static class ErrorTypeValue
{
    public const string Required = "required";
    public const string Invalid = "invalid";
    public const string InvalidFormat = "invalid_format";
    public const string Duplicate = "duplicate";
    public const string ReadOnly = "read_only";
    public const string NotAllowed = "not_allowed";
    public const string Restricted = "restricted";
    public const string InsufficientFunds = "insufficient_funds";
    public const string RequiresFundingSource = "requires_funding_source"; 
    public const string FileInvalid = "file_invalid";
}