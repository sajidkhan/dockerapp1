﻿using Metapass.Identity.Models.Api.Constants;

namespace Metapass.Identity.Models.Api;

public class Error
{
    /// <summary> The HTTP status error code. One of <see cref="ErrorCodeValue"/> </summary>
    public string Code { get; set; }
    /// <summary>
    /// The validation error types (400 bad request) One of <see cref="ErrorTypeValue"/>.
    /// </summary>
    public string? Type { get; set; }
    /// <summary> A human-readable error message, e.g. "First name is required". </summary>
    public string? Message { get; set; }
    /// <summary> Additional details about the error </summary>
    public string? DetailedMessage { get; set; }
    /// <summary> The property field of object to which 400 bad request error applies, e.g. "firstName". </summary>
    public string? Field { get; set; }
}  