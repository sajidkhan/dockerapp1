﻿using System.ComponentModel.DataAnnotations;

namespace Metapass.Identity.Models.Api;

public class Response
{
    public Response()
    {
        Errors = new List<Error>();
    }
    
    [Required]
    public bool Success { get; set; } = false;
    [Required]
    public DateTime Timestamp { get; set; } = DateTime.UtcNow;
    public List<Error>? Errors { get; set; }
    public string? Message { get; set; }
}