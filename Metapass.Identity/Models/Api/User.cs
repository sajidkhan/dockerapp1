﻿namespace Metapass.Identity.Models.Api;

public class User
{
    public Guid Id { get; set; }
    public string? FirstName { get; set; } //Can update
    public string? LastName { get; set; } //Can update
    public string? Name { get; set; }
    public string? Username { get; set; }
    public string? Email { get; set; } 
    public bool? EmailVerified { get; set; }
    public string? Phone { get; set; } 
    public bool? PhoneVerified { get; set; }
    #region Individual Account
    public Guid? AccountId { get; set; }
    public string? AccountNumber { get; set; }
    public string? AccountStatusCode { get; set; } //Can update
    public Guid? ApplicationId { get; set; }
    #endregion Individual Account
    
    public DateTime? Created { get; set; }
    public DateTime? Updated { get; set; }
    public DateTime? Deleted { get; set; }
}