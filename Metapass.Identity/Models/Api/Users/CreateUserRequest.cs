﻿using System.ComponentModel.DataAnnotations;

namespace Metapass.Identity.Models.Api.Users;

public class CreateUserRequest
{
    [Required]
    public string Email { get; set; }
    
    [Required]
    [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$", ErrorMessage = "Password must be at least 8 characters in length and contain at least 1 lower case letter, 1 upper case letter, 1 numeric character and 1 special character.")]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
    public string Password { get; set; }
}