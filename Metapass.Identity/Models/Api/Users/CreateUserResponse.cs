﻿namespace Metapass.Identity.Models.Api.Users;

public class CreateUserResponse : Response
{
    public CreateUserResponse()
    {
        User = new User();
    }

    public User? User { get; set; }
}