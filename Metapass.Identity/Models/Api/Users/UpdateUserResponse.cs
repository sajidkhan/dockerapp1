﻿namespace Metapass.Identity.Models.Api.Users;

public class UpdateUserResponse : Response
{
    public UpdateUserResponse()
    {
        User = new User();
    }

    public User? User { get; set; }
}