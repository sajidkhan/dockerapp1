﻿using System.ComponentModel.DataAnnotations;

namespace Metapass.Identity.Models.Events.Users;

public class UserAccountCreatedEvent
{
    /// <summary>
    /// Account identifier
    /// </summary>
    [Required]
    public Guid AccountId { get; set; }
    /// <summary>
    /// Account name
    /// </summary>
    [Required]
    public string Name { get; set; }
    /// <summary>
    /// Account number
    /// </summary>
    [Required]
    public string Number { get; set; }
    /// <summary>
    /// Account Status />
    /// </summary>
    [Required]
    public string Status { get; set; }
    /// <summary>
    /// Account's user identifier 
    /// </summary>
    [Required]
    public Guid UserId { get; set; }
    /// <summary>
    /// Account's application identifier
    /// </summary>
    [Required]
    public Guid ApplicationId { get; set; }
}