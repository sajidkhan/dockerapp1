﻿using System.ComponentModel.DataAnnotations;

namespace Metapass.Identity.Models.Events.Users;

public class UserRegisteredEvent
{
    [Required]
    public Guid _EventId { get; set; }
    /// <summary>
    /// User created via "Identity"
    /// </summary>
    [Required]
    public Guid UserId { get; set; }
    [Required]
    public string Username { get; set; }
    [Required]
    public string Email { get; set; }
    [Required]
    public Guid AccountId { get; set; }
    [Required]
    public string AccountNumber { get; set; }
    [Required]
    public Guid ApplicationId { get; set; }
    public string? PhoneNumber { get; set; }
    [Required]
    public DateTime Created { get; set; }
}