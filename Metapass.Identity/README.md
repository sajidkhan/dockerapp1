# Metapass.Identity

Metapass Identity Service

## Dependences 
- Metapass.Account.API
- RabbitMQ
- PostgreSQL

## Metapass Architecture Overview
Metapass platform is implemented with a microservice oriented architecture via multiple autonomous decoupled microservices (each one is independent, owning its own data) and implementing different approaches within each microservice. HTTP protocol is used for communication between the thin client apps and the microservices; An event Bus via RabbitMQ is used to support asynchronous communication for data updates propagation.

**Backend**
- ASP.NET 5.0 (C#)
- Entity Framework Core
- PostgreSQL database
- Openiddict (Authorisation and Authentication)
- Dapr

**Frontend**
- Blazor

**Metapass’s Custom Id Token**
- user_id
- first_name
- last_name
- name
- username
- email

**Metapass’s Custom (Client Credentials) Access Token**
- account_id
- account_name
- account_number
- account_profile_type
- account_status
- account_owner_account_id
- account_owner_user_id
- user_id
- application_id