﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace Metapass.Identity.Services
{
    public class EmailSender : IEmailSender
    {
        protected readonly AppSettings _appSettings;

        public EmailSender(IOptionsSnapshot<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            // Plug in your email service here to send an email.
            SendEmailExecute(subject, htmlMessage, email).Wait();
            return Task.FromResult(0);
        }

        public async Task SendEmailExecute(string subject, string htmlMessage, string email)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress("Metapass", "service@metapass.network"));
            mimeMessage.To.Add(new MailboxAddress(email, email));
            mimeMessage.Subject = subject;
            string htmlbody = @"<a href=""https://www.metapass.network"" title=""Return to Dashboard"" target=""_blank""><img src=""https://s3.amazonaws.com/metapassobjects/images/logo.png"" height=""45px""></a><br><br>";

            mimeMessage.Body = new TextPart("html")
            {
                Text = htmlbody + htmlMessage

            };

            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect(_appSettings.Connector.EmailConfig.Smtp, 587, false);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(_appSettings.Connector.EmailConfig.Username, _appSettings.Connector.EmailConfig.Password);

                client.Send(mimeMessage);
                client.Disconnect(true);
            }
        }
    }
}
