﻿using System.ComponentModel.DataAnnotations;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using Metapass.Identity.Data;
using Metapass.Identity.Helpers;
using Metapass.Identity.Models.Api;
using Metapass.Identity.Models.Api.Constants;
using Metapass.Identity.Models.Api.Users;
using Metapass.Identity.Models.Events.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Metapass.Identity.Services;

public interface IEventListenerService
{
    Task<int> UserAccountCreatedEventAsync(UserAccountCreatedEvent input);
}

public class EventListenerService : IEventListenerService
{
    private readonly ApplicationDbContext _context;
    private readonly AppSettings _appSettings;
    private readonly IDistributedCache _distributedCache;
    private readonly UserManager<DataModels.User> _userManager;
    private readonly IEmailSender _emailSender;
    private readonly IWebHostEnvironment _hostingEnvironment;
    private readonly ILogger<EventListenerService> _logger;

    public EventListenerService(ApplicationDbContext dbContext, IOptionsSnapshot<AppSettings> appSettings,
        IDistributedCache distributedCache, UserManager<DataModels.User> userManager, IEmailSender emailSender,
        IWebHostEnvironment hostingEnvironment, ILogger<EventListenerService> logger)
    {
        _context = dbContext;
        _appSettings = appSettings.Value;
        _distributedCache = distributedCache;
        _userManager = userManager;
        _emailSender = emailSender;
        _hostingEnvironment = hostingEnvironment;
        _logger = logger;
    }
    
    public async Task<int> UserAccountCreatedEventAsync(UserAccountCreatedEvent input)
    {
        UserAccountCreatedEvent response = new UserAccountCreatedEvent();

        var userData = _context.Users.FindAsync(input.UserId).Result;

        //User not found
        if (userData == null)
        {
            _logger.LogError("UserAccountStatusEvent"+ input.UserId.ToString() + "user not found");
            //error is logged and message is dropped
            return 404;
        }

        try
        {
            //User found, sync user
            if (userData.Id != Guid.Empty)
            {
                //Sync Account status
                userData.AccountStatusCode = input.Status;

                userData.Synced = userData.Synced ?? DateTime.UtcNow;
                userData.Updated = DateTime.UtcNow;

                _context.Update(userData);
                await _context.SaveChangesAsync();

                return 200;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return 500;
        }

        return 500;
    }
}