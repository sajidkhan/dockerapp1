﻿using System.ComponentModel.DataAnnotations;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using Metapass.Identity.Data;
using Metapass.Identity.Helpers;
using Metapass.Identity.Models.Api;
using Metapass.Identity.Models.Api.Constants;
using Metapass.Identity.Models.Api.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Metapass.Identity.Services;

public interface IIdentityRepository
{
    Task<(int, CreateUserResponse)> CreateUserAsync(CreateUserRequest request);
    Task<(int, UpdateUserResponse)> UpdateUserAsync(Guid userId, UpdateUserRequest request);
}

public class IdentityRepository : IIdentityRepository
{
    private readonly ApplicationDbContext _context;
    private readonly AppSettings _appSettings;
    private readonly IDistributedCache _distributedCache;
    private readonly UserManager<DataModels.User> _userManager;
    private readonly IEmailSender _emailSender;
    private readonly IWebHostEnvironment _hostingEnvironment;
    private readonly ILogger<IdentityRepository> _logger;

    public IdentityRepository(ApplicationDbContext dbContext, IOptionsSnapshot<AppSettings> appSettings,
        IDistributedCache distributedCache, UserManager<DataModels.User> userManager, IEmailSender emailSender,
        IWebHostEnvironment hostingEnvironment, ILogger<IdentityRepository> logger)
    {
        _context = dbContext;
        _appSettings = appSettings.Value;
        _distributedCache = distributedCache;
        _userManager = userManager;
        _emailSender = emailSender;
        _hostingEnvironment = hostingEnvironment;
        _logger = logger;
    }

    public async Task<(int, CreateUserResponse)> CreateUserAsync(CreateUserRequest request)
    {
        CreateUserResponse response = new CreateUserResponse();

        #region Check Required

        response.Errors = ValidateModelErrorMapper(request);

        //Return errors with required fields
        if (response.Errors.Count > 0)
        {
            response.Message = "Invalid request error.";
            return (400, response);
        }

        #endregion Check Required

        //Check new email already in the database and registered to another user?

        #region Find Email Exist

        var userResultData = _userManager.FindByEmailAsync(request.Email.ToLower()).Result;

        if (userResultData.Id != Guid.Empty)
        {
            //Email already exist
            response.Errors.Add(new Models.Api.Error
            {
                Code = ErrorCodeValue.BadRequest,
                Type = ErrorTypeValue.Invalid,
                Message = "Email is invalid, email already registered to an existing user.",
                Field = "email"
            });

            return (400, response);
        }

        #endregion Find Email Exist

        try
        {
            string newUsername = await GenerateUsernameAsync();
            Guid userId = Guid.NewGuid();
            Guid userAccountApplicationId = Guid.NewGuid();

            var userData = new DataModels.User
            {
                Id = userId,
                UserName = newUsername,
                Email = request.Email.ToLower(),
                Name = request.Email.ToLower(),
                //Account
                AccountId = userId,
                AccountNumber = await GenerateMetapassAccountNumberAsync(),
                AccountStatusCode = "incomplete",
                ApplicationId = userAccountApplicationId,
                //ExternalId 
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
                EmailConfirmed = true,
                NormalizedEmail = request.Email.ToUpper(),
                NormalizedUserName = newUsername.ToUpper(),
            };
            var result = await _userManager.CreateAsync(userData, request.Password);

            if (result.Succeeded)
            {
                response.User = MapUserDataToUserModel(userData);
                response.Success = true;

                #region Publish UserRegisterEvent

                if (_appSettings.Event.UserRegisteredEventEnabled)
                {
                    using (var httpClient = new System.Net.Http.HttpClient())
                    {
                        var byteArray = Encoding.UTF8.GetBytes(_appSettings.OpenIdConnect.ApiClientId + ":" + _appSettings.OpenIdConnect.ApiClientSecret);

                        httpClient.DefaultRequestHeaders.Authorization =
                            new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                        var requestUri = new Uri(_appSettings.Event.UserRegisteredEventEndpoint, UriKind.Absolute);

                        //Event will register the user, followed by creating the user's account via Metapass.Account
                        Metapass.Identity.Models.Events.Users.UserRegisteredEvent userRegisteredEvent = new Metapass.Identity.Models.Events.Users.UserRegisteredEvent
                        {
                            _EventId = Guid.NewGuid(),
                        
                            //User
                            UserId = userData.Id,
                            Username = userData.UserName,
                            Email = userData.Email,
                            //PhoneNumber = userData.PhoneNumber,
                            //Account
                            AccountId = (Guid) userData.AccountId,
                            AccountNumber = userData.AccountNumber,
                            ApplicationId = (Guid) userData.ApplicationId,
                            //ExternalId
                            Created = (DateTime)userData.Created
                        };
                    
                        string json = JsonConvert.SerializeObject(userRegisteredEvent);
                        StringContent data = new StringContent(json, Encoding.UTF8, "application/json");
                    
                        //POST Method
                        var httpResponse = await httpClient.PostAsync(requestUri, data);

                        if (httpResponse.IsSuccessStatusCode)
                        {
                        }
                    }
                }

                #endregion Publish UserRegisterEvent

                if (_hostingEnvironment.EnvironmentName == "Development")
                {
                    _logger.LogInformation("User created a new account with password. {Username} | {Email}",
                        userData.UserName, userData.Email);
                }
                else
                {
                    await _emailSender.SendEmailAsync(userData.Email, "Welcome to " + _appSettings.AppName,
                        $"Thank you for registering with " + _appSettings.AppName +
                        ", please take a few moments to complete your online profile.<br /><br />" +
                        "You can use either of these as your login username:<br />" +
                        $"- {userData.UserName}" + "<br />" +
                        $"- {userData.Email}");
                }

                return (200, response);
            }
        }
        catch (Exception e)
        {
        }


        return (500, response);
    }
    public async Task<(int, UpdateUserResponse)> UpdateUserAsync(Guid userId, UpdateUserRequest request)
    {
        UpdateUserResponse response = new UpdateUserResponse();

        #region Find UserById

        //Change Name?
        if (string.IsNullOrEmpty(request.FirstName) && !string.IsNullOrEmpty(request.LastName))
        {
            response.Errors.Add(new Models.Api.Error
            {
                Code = ErrorCodeValue.BadRequest,
                Type = ErrorTypeValue.Required,
                Message = "First name is required.",
                Field = "firstName"
            });

            return (400, response);
        }

        if (!string.IsNullOrEmpty(request.FirstName) && string.IsNullOrEmpty(request.LastName))
        {
            response.Errors.Add(new Models.Api.Error
            {
                Code = ErrorCodeValue.BadRequest,
                Type = ErrorTypeValue.Required,
                Message = "Last name is required.",
                Field = "LastName"
            });

            return (400, response);
        }

        var userData = _userManager.FindByIdAsync(userId.ToString()).Result;

        //User not found
        if (userData.Id == Guid.Empty)
        {
            response.Errors.Add(new Models.Api.Error
            {
                Code = ErrorCodeValue.BadRequest,
                Type = ErrorTypeValue.Invalid,
                Message = "User Id is invalid.",
                Field = "userId"
            });

            return (400, response);
        }

        #endregion Find UserById
        if (!string.IsNullOrEmpty(request.FirstName) && !string.IsNullOrEmpty(request.LastName))
        {
            userData.FirstName = request.FirstName;
            userData.LastName = request.LastName;

            userData.Name = request.FirstName + "" + request.LastName;
        }

        //Check new email already in the database and registered to another user?

        #region Find Email Exist

        var userDataResult = _userManager.FindByEmailAsync(request.Email.ToLower()).Result;

        if (userDataResult.Id != Guid.Empty && userDataResult.Id != userData.Id)
        {
            response.Errors.Add(new Models.Api.Error
            {
                Code = ErrorCodeValue.BadRequest,
                Type = ErrorTypeValue.Invalid,
                Message = "Email is invalid, email already registered to an existing user.",
                Field = "email"
            });

            return (400, response);
        }

        userData.Email = request.Email ?? userData.Email;

        #endregion Find Email Exist

        try
        {
            if (userData.Id != Guid.Empty)
            {
                userData.Updated = DateTime.UtcNow;

                _context.Update(userData);
                await _context.SaveChangesAsync();

                response.User = MapUserDataToUserModel(userData);
                response.Success = true;
                return (200, response);
            }
        }
        catch (Exception ex)
        {
            response.Errors.Add(new Models.Api.Error
            {
                Code = ErrorCodeValue.ServerError,
                Type = ErrorTypeValue.Invalid,
                Message = ex.Message,
                Field = "Internal Server Error"
            });

            return (500, response);
        }

        return (500, response);
    }

    #region ValidationHelpers

    public List<Error> ValidateModelErrorMapper(object instance)
    {
        List<KeyValuePair<string, string>> kvpErrors = new List<KeyValuePair<string, string>>();
        List<Error> errors = new List<Error>();

        kvpErrors = ValidateModel(instance);
        if (kvpErrors.Count != 0)
        {
            foreach (KeyValuePair<string, string> kvp in kvpErrors)
            {
                errors.Add(new Error
                {
                    Code = Models.Api.Constants.ErrorCodeValue.BadRequest,
                    Type = Models.Api.Constants.ErrorTypeValue.Invalid,
                    Field = ToCamelCase(kvp.Key),
                    Message = kvp.Value,
                    DetailedMessage = ""
                });
            }
        }

        return errors;
    }

    /// <summary>
    /// Validate a class model and return key value pair error list
    /// </summary>
    /// <param name="instance">Class model</param>
    /// <returns>Key value pair list</returns>
    private List<KeyValuePair<string, string>> ValidateModel(object instance)
    {
        List<KeyValuePair<string, string>> errors = new List<KeyValuePair<string, string>>();
        ValidationContext vContext = new ValidationContext(instance, null, null);
        List<ValidationResult> vResults = new List<ValidationResult>();
        bool isRequestModelValid = Validator.TryValidateObject(instance, vContext, vResults, true);

        if (!isRequestModelValid)
        {
            foreach (ValidationResult vr in vResults)
            {
                errors.Add(new KeyValuePair<string, string>(ToCamelCase(vr.MemberNames.First()), vr.ErrorMessage));
            }
        }

        return errors;
    }

    /// <summary>
    /// Convert the string to Pascal case.
    /// </summary>
    /// <param name="textInput"></param>
    /// <returns></returns>
    private static string ToPascalCase(string textInput)
    {
        string result = string.Empty;

        try
        {
            result = Regex.Replace(textInput, "(?:^|_)(.)", match => match.Groups[1].Value.ToUpper());
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return result;
    }

    /// <summary>
    /// Convert the string to camel case.
    /// </summary>
    /// <param name="textInput"></param>
    /// <returns></returns>
    private static string ToCamelCase(string textInput)
    {
        string result = string.Empty;
        var myTextInput = ToPascalCase(textInput);

        try
        {
            if (myTextInput.Contains('.'))
            {
                result = Regex.Replace(myTextInput, @"\b\p{Lu}", m => m.Value.ToLower());
            }
            else
            {
                result = myTextInput.Substring(0, 1).ToLower() + myTextInput.Substring(1);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return result;
    }

    #endregion ValidationHelpers

    #region Helpers

    private User MapUserDataToUserModel(DataModels.User userData)
    {
        var user = new User
        {
            Id = userData.Id,
            FirstName = userData.FirstName,
            LastName = userData.LastName,
            Name = userData.Name,
            Username = userData.UserName,
            Email = userData.Email,
            EmailVerified = userData.EmailConfirmed,

            //Account
            AccountId = userData.AccountId,
            AccountNumber = userData.AccountNumber,
            AccountStatusCode = userData.AccountStatusCode,
            ApplicationId = userData.ApplicationId,

            Created = userData.Created,
            Updated = userData.Updated,
            Deleted = userData.Deleted
        };

        return user;
    }

    #endregion Helpers

    #region AccountHelpers

    private async Task<string> GenerateUsernameAsync()
    {
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//TODO In the future you may need to increase this
        char[] stringChars = new char[12];
        string finalString = null;
        bool valid = false;

        do
        {
            Random random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            finalString = new String(stringChars);

            var user = await _userManager.FindByNameAsync(finalString);

            if (user == null)
            {
                valid = true;
            }
        } while (valid == false);

        return finalString;
    }

    /// <summary>
    /// Generate Metapass individual account number
    /// </summary>
    /// <returns></returns>
    private async Task<string> GenerateMetapassAccountNumberAsync()
    {
        int startNumber = 6; //6 is for individual account, 7 is for business account

        string newAccountNumber = null;
        newAccountNumber = GenerateAccountNumber(startNumber.ToString());
        bool duplicateAccountNumberFound = false;

        do
        {
            duplicateAccountNumberFound =
                await _context.Users.AnyAsync(q => q.AccountNumber == newAccountNumber);

            newAccountNumber = GenerateAccountNumber(startNumber.ToString());
        } while (duplicateAccountNumberFound);

        return newAccountNumber;
    }

    /// <summary>
    /// Generate 16 digit account number with ending check digit.
    /// </summary>
    /// <param name="accountStartNumber"></param>
    /// <returns></returns>
    private string GenerateAccountNumber(string accountStartNumber = "7")
    {
        string finalResult = null;
        string result = null;

        Random generator = new Random();
        string value1 = generator.Next(0, 9999999).ToString("D7");
        string value2 = generator.Next(0, 99999999).ToString("D8");
        int checkDigit = 0;

        result = accountStartNumber + value1 + value2;

        checkDigit = GenerateChecksumDigit(result);

        finalResult = result + checkDigit.ToString();

        return finalResult;
    }

    /// <summary>
    /// Generate valid numbers by adding a checksum digit using luhn algorithm.
    /// </summary>
    /// <param name="Sequence">Generate a checksum digit from sequence.</param>
    /// <param name="ssCheckDigit">Luhn checksum digit</param>
    private int GenerateChecksumDigit(string Sequence)
    {
        int CheckDigit = 0;

        // convert the sequence to int[]
        char[] charDigits = Sequence.ToCharArray();
        int[] digits = new int[charDigits.Length];

        for (int i = 0; i < charDigits.Length; i++)
        {
            digits[i] = charDigits[i] - '0';
        }

        // our algorithm begins here
        int sum = 0;
        int length = digits.Length;
        for (int i = 0; i < length; i++)
        {
            // get digits in reverse order
            int digit = digits[length - i - 1];

            // multiply every 2nd number with 2
            if (i % 2 == 1)
            {
                digit *= 2;
            }

            sum += digit > 9 ? digit - 9 : digit;
        }

        CheckDigit = sum % 10;

        return CheckDigit;
    } // GenerateChecksumDigit

    #endregion AccountHelpers
}