using Metapass.Identity.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Metapass.Identity.Services;
using Microsoft.AspNetCore.Identity.UI.Services;
using static Metapass.Identity.Data.ApplicationDbContext;
using static OpenIddict.Abstractions.OpenIddictConstants;
using Metapass.Identity.Services;

namespace Metapass.Identity
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
                options.UseOpenIddict<ApplicationClient, ApplicationAuthorization, ApplicationScope, ApplicationToken, Guid>();
            });
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddIdentity<DataModels.User, DataModels.Role>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
            })
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            #region IdentityOptions
            // Configure Identity https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });
            #endregion IdentityOptions

            services.AddControllersWithViews();
            services.AddRazorPages();
 
            #region AppSettings
            services.Configure<AppSettings>(Configuration);
            services.AddOptions();
            services.AddTransient<IEventListenerService, EventListenerService>();
            services.AddTransient<IIdentityRepository, IdentityRepository>();
            #endregion AppSettings

            #region Application services
            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            #endregion Application services

            #region OpenIddict
            // Configure Identity to use the same JWT claims as OpenIddict instead
            // of the legacy WS-Federation claims it uses by default (ClaimTypes),
            // which saves you from doing the mapping in your authorization controller.
            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = Claims.Role;
            });

            services.AddOpenIddict()
                // Register the OpenIddict core components.
                .AddCore(options =>
                {
                    // Configure OpenIddict to use the Entity Framework Core stores and models.
                    // Note: call ReplaceDefaultEntities() to replace the default OpenIddict entities.
                    options.UseEntityFrameworkCore()
                           .UseDbContext<ApplicationDbContext>()
                           .ReplaceDefaultEntities<ApplicationClient, ApplicationAuthorization, ApplicationScope, ApplicationToken, Guid>();
                })

                // Register the OpenIddict server components.
                .AddServer(options =>
                {
                    // Enable the authorization, logout, token and userinfo endpoints.
                    options.SetAuthorizationEndpointUris("/connect/authorize")
                           .SetLogoutEndpointUris("/connect/logout")
                           .SetTokenEndpointUris("/connect/token")
                           .SetUserinfoEndpointUris("/connect/userinfo")
                           .SetIntrospectionEndpointUris("/connect/introspect");

                    // Enable the client credentials flow.
                    options.AllowClientCredentialsFlow();
                    // Enable the code and refresh token flows
                    options.AllowAuthorizationCodeFlow()
                           .AllowRefreshTokenFlow();

                    // Mark the "email", "profile" and "roles" scopes as supported scopes.
                    options.RegisterScopes(GetRequiredScopes());

                    // Register the signing and encryption credentials.
                    if (Environment.IsDevelopment())
                    {
                        options.AddDevelopmentEncryptionCertificate()
                               .AddDevelopmentSigningCertificate();
                    }
                    else
                    {
                        options.AddEphemeralEncryptionKey()
                               .AddEphemeralSigningKey();
                    }

                    // Register the ASP.NET Core host and configure the ASP.NET Core-specific options.
                    options.UseAspNetCore()
                           .EnableAuthorizationEndpointPassthrough()
                           .EnableLogoutEndpointPassthrough()
                           .EnableStatusCodePagesIntegration()
                           .EnableTokenEndpointPassthrough()
                           .EnableAuthorizationRequestCaching()
                           .EnableLogoutRequestCaching()
                           .DisableTransportSecurityRequirement();
                })

                // Register the OpenIddict validation components.
                .AddValidation(options =>
                {
                    // Import the configuration from the local OpenIddict server instance.
                    options.UseLocalServer();

                    // Register the ASP.NET Core host.
                    options.UseAspNetCore();
                });
            #endregion OpenIddict

            #region  Sessions
            services.AddSession(o =>
            {
                o.Cookie.Name = "metapass";
                o.Cookie.SameSite = SameSiteMode.None;
                o.Cookie.HttpOnly = true;
                o.IdleTimeout = TimeSpan.FromMinutes(60);
            });
            #endregion Sessions
            
            #region HealthCheck
            services.AddHealthChecks()
                .AddNpgSql(
                Configuration["ConnectionStrings:DefaultConnection"],
                name: "identity-database-health",
                tags: new string[] { "database" });
            #endregion HealthCheck
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            // This cookie policy fixes login issues with Chrome 80+ using HTTP
            app.UseCookiePolicy(new CookiePolicyOptions { MinimumSameSitePolicy = SameSiteMode.Lax });
            
            //Seed Database
            ConfigurationDbContextSeed configurationDbContextSeed = new ConfigurationDbContextSeed();
            configurationDbContextSeed.IntialiseAsync(app, Configuration);

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            #region Sessions
            app.UseSession();
            #endregion Sessions

            #region HealthCheck
            app.UseHealthChecks("/Health");
            #endregion HealthCheck

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }

        #region Helpers
        private string[] GetRequiredScopes()
        {
            List<string> returnedPermissions = new List<string>();

            returnedPermissions.Add(Scopes.Email);
            returnedPermissions.Add(Scopes.Profile);
            returnedPermissions.Add(Scopes.Roles);

            string platformscopesString = Configuration["OpenIdConnect:PlatformScopes"];
            string[] platformScopes = platformscopesString.ToLower().Split('|');

            foreach (var scopeItem in platformScopes)
            {
                returnedPermissions.Add(scopeItem);
            }

            return returnedPermissions.ToArray();
        }
        #endregion Helpers
    }
}
